# Prometheus Aufträge

[TOC]

Im Folgenden werden Aufträge definiert, welche Ihnen helfen sollen, sich in das Thema zu vertiefen. Mit jedem Teil kann man sich mehr oder weniger stark beschäftigen und so entsprechendes Know-How aneignen. Grundsätzlich sollten Sie die einfachen Ziele erreichen, die erweiterten sind eher freiwillig und benötigen vertieftes Eintauchen in das Thema. 

**Ziele:** 

- Sie verstehen die unterschiedlichen Konzepte zwischen Prometheus und InfluxDB
- Sie können *Prometheus* installieren - manuell und evtl. zusätzlich via Skript/Cloud-Init (infrastructure as code)
- Sie können *Prometheus* einsetzen um *metrics* zu lesen und auszugeben.

**Erweiterte Ziele:**

- Alertmanager und Benachrichtigungen senden.



**Achtung**: Dokumentieren Sie Ihre Schritte in Ihrem Journal, so dass Sie wiederholbar sind und die Lehrperson diese einsehen und so helfen kann.



## A) Cloud-Init Installation

Installieren Sie [*Prometheus* nach dieser Anleitung](Installation.md) und verwenden Sie gleich Cloud-Init und die entsprechende Anleitung in den Links.



## B) Installation testen und erweitern

Erklären Sie was Scrapes sind (z.B. im Lernjournal).

Die Anleitung für Cloud-Init hat Ihnen gleich Scrapes, Custom Rules und Alerts hinzugefügt.

- Schauen Sie sich die gegebenen Scrapes an und erklären Sie was diese tun.
- Schauen Sie sich die [Recording Rules](https://prometheus.io/docs/prometheus/latest/configuration/recording_rules/) und [Alerting Rules](https://prometheus.io/docs/prometheus/latest/configuration/alerting_rules/) an und erklären Sie was diese tun. 

Interessant werden die gesammelten Daten natürlich erst, wenn man sie auch visualisieren kann. Öffnen Sie die beiden Ports aus dem Cloud-Init (9090 und 9100) auf der Firewall und sehen Sie sich im Browser auf diesen Ports um. Sie sehen, dass Sie die gesammelten Daten visualisieren können.



## C) Python Code  

Erstellen Sie ein Python Code, welcher den Endpoint */metrics* bereitstellt. Sie sollten die entsprechenden Frameworks aus den anderen Modulen kennen. Sie definieren selbst, welche Werte der Endpoint zurückliefern soll (2-6 Werte sollten reichen zum testen). Es können auch einfach Zufallszahlen sein in einem gewissen Range. Konsumieren Sie den Endpoint nun mit Prometheus, so dass die entsprechenden Werte anschliessend sichtbar werden. Sie können den Python Code entweder auf der gleichen Maschine hosten wie Prometheus oder auf einer anderen. 

Erstellen Sie Recording Rules und Alerts, die auf Ihren (Zufalls-)Werten basieren, so dass u. U. ein Alert ausgelöst wird in Prometheus.



## D) Erweiterung: Alertmanager

Nur weil Prometheus einen Alert schickt, bedeutet dies noch nicht, dass der Alert auch Beachtung findet. Im UI (Webinterface) sollte der Alert aber sichtbar sein.

Eine weitere Komponente notwendig - [Der Alertmanager](https://prometheus.io/docs/alerting/latest/alertmanager/) - stellt sicher, dass der Alert auch verschickt wird. Richten Sie den Alertmanager ein und schicken Sie sich eine z.B. E-Mail, falls ein Alarm ausgelöst wird - z.B. aufgrund Ihrer Zufallszahlen aus dem Python Code.





