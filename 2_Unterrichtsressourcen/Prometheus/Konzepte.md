# Konzepte von Key-Value Datenbanken

[TOC]

## Was ist eine Time Series Datenbank

Eine Time Series Datenbank (TSDB)  funktioniert ähnlich wie eine Key-Value Datenbank mit der Eigenheit, dass die Schlüssel Zeitangaben sind. An diesen Zeitangaben können wiederum beliebige Daten gespeichert werden. Ein grosser Unterschied besteht darin, dass durchaus auch nach Inhalten gefiltert werden kann. Dies hängt aber auch von der Funktionsweise der spezifischen Datenbank ab. 

Im Folgenden vertiefen wir uns in zwei Datenbanken, die unterschiedliche Konzepte verfolgen

## InfluxDB von influxdata

Daten werden auf die Datenbank gepushed wie man dies von relationalen Datenbanken kennt. Abfragen können natürlich ebenfalls abgesetzt werden um Daten wieder auszulesen. Bei den relationalen Datenbanken kennt man die Limitierung, dass in eine Tabelle entweder schnell geschrieben wird oder schnell gelesen wird, aber nicht beides. Dies kommt daher, dass für die höhere Geschwindigkeit der Lese-Zugriffe Indexierungen  auf mehreren Spalten erstellt werden können. Aber diese Massnahme macht das Einfügen langsamer, da alle Indexes aktualisiert werden müssen.

InfluxDB löst dieses Problem, indem das Lesen und Schreiben getrennt werden. Dies bedeutet, dass im Hintergrund grosse Verwaltungsaufgaben stattfinden müssen.

![Figure_1_InfluxDB_3-0_Architecture](_res/Figure_1_InfluxDB_3-0_Architecture.png)

Quelle: https://www.influxdata.com/blog/influxdb-3-0-system-architecture/

### Use Cases

**Log-Informationen** 

Sie schicken Log-Informationen zu der Datenbank, z.B. für eine Web-Applikation. Jeder Request wird dabei an die Datenbank geschickt, inkl. der Header, Methode und aufgerufene URL. Bei sehr hohem Load der Web-Applikation, muss die Datenbank die Anfragen in Nano-Sekunden aufteilen und speichern können. 

**Finanzdaten**

Auch Finanzdaten, z.B. Daten von Börsentitel oder Cryptos können so gespeichert werden. Diese Daten sind oft in verschiedenen Zeitintervallen verfügbar bis runter zu 1 Sekunden-Schritte. Trotzdem müssen die Daten auch für eine Auswertung schnell verfügbar sein. 



## Prometheus

Daten werden nicht via Push oder Queries in die Prometheus Datenbank geschrieben, sondern, Prometheus holt sich die Daten selbst. Dazu haben Applikationen eine URL - meist */metrics* - implementiert, die in Prometheus eingebunden werden kann. Es geht bei Prometheus auch gar nicht darum, dass viele Log-Inhalte gesammelt werden, sondern die Datenbank versteht sich als Überwachungstool. 

Während Log-Informationen einzeln in die InfluxDB geschrieben werden, sammelt Prometheus in regelmässigen Inhalten zusammengefasste Daten, z.B. wie oft wurde eine spezifische URL aufgerufen - ohne, dass die Details eines einzelnen Aufrufs ebenfalls gespeichert wird. Ein wichtiger Fokus von Prometheus liegt bei Alerts und Notifications, die konfiguriert und ausgelöst werden können - aufgrund der Daten. 

![TimeSeries](_res/TimeSeries.png)

Daten werden hoch-komprimiert gespeichert und für Abfragen optimiert, während Daten nur alle paar Sekunden von den Metrics-URLs gelesen werden.



![prometheus_architecture](_res/prometheus_architecture.png)

Quelle: https://prometheus.io/docs/introduction/overview/



### Use Cases

Anwendung findet Prometheus in allen möglichen Umgebungen in denen Systeme überwacht werden sollen. Prometheus arbeitet gut mit Kubernetes zusammen und ist darum für uns in diesem Modul relevant.



## Populäre Key-Value-Datenbanken

- [Prometheus](https://prometheus.io/)
- [InfluxDB](https://www.influxdata.com/)
- [Viele andere...](https://db-engines.com/en/ranking/time+series+dbms)



## Quellen

- 1 [Amazon: What Is a Key-Value Database?](https://aws.amazon.com/nosql/key-value/) 
- 2 [Influxdata: InfluxDB vs Prometheus](https://www.influxdata.com/comparison/influxdb-vs-prometheus/) 



