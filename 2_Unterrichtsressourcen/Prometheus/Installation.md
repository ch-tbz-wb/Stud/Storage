# Prometheus Installation

[TOC]

## Installation

In den Quellen finden Sie Anweisungen zur Installation von *Prometheus*. Speziell die zweite Quelle enthält eine gute Anleitung, die auch einen Service und Beispiel-Scrapes für *Prometheus* erstellt. 

In AWS verwenden Sie folgende Einstellungen:

- Typ: t2.Medium
- Speichergrösse: 20GB
- Verwenden Sie das [Basis Cloud-Init](../cloud-init-basis.yaml) , so dass die Lehrperson immer auch Ihre Instanzen zugreifen kann. Vergessen Sie nicht Ihren eigenen Schlüssel hinzuzufügen, damit auch Sie sich anschliessend verbinden können. Falls Sie den Umgang mit SSH auffrischen möchten, finden Sie [hier](../SshPublicPrivateKey.md) eine kurze Übersicht

**Quellen**

- <https://prometheus.io/docs/prometheus/latest/installation/>
- <https://medium.com/@ronaks1907/how-to-set-up-prometheus-using-cloud-init-script-b58a34c3d058>
- <https://blog.fourninecloud.com/what-is-prometheus-and-how-to-install-and-configure-it-on-linux-server-9b5f88685451> (Ausführliche manuelle Installation)
- <https://prometheus.io/download/>



## Überprüfung der Installation

1. Warten Sie bis die Installation abgeschlossen ist. Überprüfen Sie die Cloud-Init Log-Datei: */var/log/cloud-init-output.log*, falls Sie ein Cloud-Init erstellt haben.
2. Testen Sie, ob die beide Services laufen indem Sie den Status aufrufen und überprüfen Sie auch die Version der beiden Pakete
   1. `sudo systemctl status prometheus`
   2. `sudo systemctl status prometheus-node-exporter.service`
   3. `prometheus --version`
   4. `prometheus-node-exporter --version`



