# etcd Installation

[TOC]

## Installation

In den Quellen finden Sie Anweisungen zur Installation von *etcd*. Speziell die zweite Quelle enthält eine gute Anleitung, die auch einen Service für *etcd* erstellt. Auch wenn eine einfache Installation via Package Manager möglich ist, sollten Sie eine der anderen beiden Varianten wählen, da der Package Manager nicht die neuste Version installiert.

In AWS verwenden Sie folgende Einstellungen:

- Typ: t2.Mikro
- Speichergrösse: 8GB
- Verwenden Sie das [Basis Cloud-Init](..\cloud-init-basis.yaml) , so dass die Lehrperson immer auch Ihre Instanzen zugreifen kann. Vergessen Sie nicht Ihren eigenen Schlüssel hinzuzufügen, damit auch Sie sich anschliessend verbinden können. Falls Sie den Umgang mit SSH auffrischen möchten, finden Sie [hier](../SshPublicPrivateKey.md) eine kurze Übersicht
- Firewall: Die korrekten Ports werden Sie später öffnen müssen, wenn Sie von aussen auf die Datenbank zugreifen möchten.


**Quellen**

- <https://etcd.io/docs/v3.5/install/>
- <https://linuxconfig.org/how-to-install-etcd-on-ubuntu>
- <https://github.com/etcd-io/etcd/releases/>



## Überprüfung der Installation

1. Warten Sie bis die Installation abgeschlossen ist. Überprüfen Sie die Cloud-Init Log-Datei: */var/log/cloud-init-output.log*, falls Sie ein Cloud-Init erstellt haben.
2. Testen Sie, ob Sie sich mit der etcd verbinden können mit den folgenden Befehlen: 
   1. `etcd --version`
   2. `etcdctl version`
   3. `etcdutl version`



