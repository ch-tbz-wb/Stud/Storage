# etcd Aufträge

[TOC]

Im Folgenden werden Aufträge definiert, welche Ihnen helfen sollen, sich in das Thema zu vertiefen. Mit jedem Teil kann man sich mehr oder weniger stark beschäftigen und so entsprechendes Know-How aneignen. Grundsätzlich sollten Sie die einfachen Ziele erreichen, die erweiterten sind eher freiwillig und benötigen vertieftes Eintauchen in das Thema. 

**Ziele:** 

- Sie verstehen was *etcd* ist
- Sie können *etcd* installieren - manuell und evtl. zusätzlich via Skript/Cloud-Init (infrastructure as code)
- Sie können einfache Werte schreiben, lesen, verändern und löschen mit Hilfe der Konsole
- Sie können Werte beobachten und auf Änderungen reagieren mit Hilfe der Konsole
- Sie können mit Python einfache Werte lesen, verändern, schreiben und löschen.

**Erweiterte Ziele:**

- Sie können einen Cluster installieren mit mindestens 3 Teilnehmern.
- Authentifizierung



**Achtung**: Dokumentieren Sie Ihre Schritte in Ihrem Journal, so dass Sie wiederholbar sind und die Lehrperson diese einsehen und so helfen kann.



## A) Manuelle Installation

Nachdem Sie sich in AWS zurechtgefunden haben, können Sie relativ einfach Instanzen erstellen und [*etcd* installieren](Installation.md)



## B) Befehle Testen

Verwenden Sie nun die Konsole auf dem Server, um Befehle zu testen, z.B. 

- Schreiben von Werten
- Lesen von Werten
- Löschen von Werten
- Beobachten von Werten. Sie benötigen eine zweite SSH Konsole dazu.

Sie finden auf der [offiziellen Webseite die verschiedenen Befehle](https://etcd.io/docs/v3.5/dev-guide/interacting_v3/)



## C) Authentifizierung

Sie haben sicherlich bemerkt, dass Sie sich verbinden können, ohne, dass Sie sich anmelden müssen. [Aktivieren Sie die Authentifizierung nachdem Sie entsprechende Benutzer erstellt haben](https://etcd.io/docs/v3.5/op-guide/authentication/rbac/). Dies können Sie dann gleich auch gleich in die Cloud-Init Konfiguration mit einbauen (siehe nächste Aufgabe)



## D) Cloud-Init Installation

Nachdem Ihre manuelle Installation erfolgreich ist, können Sie nun relativ einfach eine Cloud-Init Konfiguration erstellen, die Ihnen die manuellen Schritte abnimmt. Verwenden Sie das [Basis Cloud-Init](../cloud-init-basis.yaml) und erweitern Sie es, so dass der SSH-Schlüssel der Lehrperson ebenfalls drin ist. 



## E) Python Code

Schreiben Sie ein kleines Python Programm, welches Werte lesen und schreiben kann. Sie benötigen lediglich 4-8 Zeilen Code, abhängig davon was Sie alles aufrufen möchten. Natürlich benötigen Sie auch eine Bibliothek, z.B. [python-etcd3](https://python-etcd3.readthedocs.io/en/latest/readme.html). 

Falls Sie Fehlermeldungen wegen dem Buffer haben, können Sie die Bibliothek [entsprechend downgraden mit dem Befehl](https://stackoverflow.com/questions/72441758/typeerror-descriptors-cannot-not-be-created-directly)  `pip install protobuf==3.20.*`.

Den Code können Sie problemlos auf dem Server selbst ausführen, nachdem Sie Python ebenfalls installieren. Allerdings sollten Sie die Datenbank von aussen aufrufen - also von Ihrem lokalen System, da die App und Datenbank kaum auf der gleichen Umgebung läuft. Die Firewall und Sicherheit der Pakete auf dem Server lassen dies nicht zu. Schauen Sie sich folgend Punkte an:

- Firewall: Welcher Port wird benötigt für den Zugriff auf die Datenbank? Wo können Sie diesen Port öffnen?
- Paket: etcd selbst lässt keine Verbindungen von aussen zu. Dies muss konfiguriert werden. Schauen Sie sich [die Seite mit den Informationen zu dem Cluster](https://etcd.io/docs/v3.5/op-guide/clustering/) an, speziell die beiden Werte `advertise-client-urls` und `listen-client-urls` .



## Erweiterung I: Cluster

Sie haben ja bereits die Seite mit den Informationen zu dem Cluster gesehen. Richten Sie einen Cluster mit drei Teilnehmern ein. 



## Erweiterung II: Transport Security

Ihre Verbindungen sind noch unsicher. Jederzeit kann jemand Daten abfangen, die gesendet werden. Dies ist  nicht sonderlich tragisch, wenn die Server nur innerhalb des Netzwerkes miteinander kommunizieren (aber es immer noch eine Schwachstelle!). Aber wenn Clients von ausserhalb des Netzwerkes mit dem Cluster kommunizieren können, muss dies definitiv behoben werden. [Weitere Informationen finden Sie in der offziellen Dokumentation](https://etcd.io/docs/v3.5/op-guide/security/). Das Zertifikat können Sie selbst signieren.



## Erweiterung III: Wartung

Es gibt noch verschiedene Tools zum Verkürzen von Logfiles, Defragmentierung, Backup und Restore, etc. Sie dürfen natürlich frei diese Optionen testen.

