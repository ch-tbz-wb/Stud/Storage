# Konzepte von Key-Value Datenbanken

[TOC]

Die Quellen enthalten weitere Details zu den folgenden zusammengefassten Konzepten. Die sollten unbedingt gelesen werden. 



## Was ist eine Key-Value Datenbank

Eine Key-Value Datenbank speichert Werte, die mit einem entsprechenden Schlüssel verknüpft sind. Dies klingt zunächst einfach, aber wenn man bedenkt, dass ein Wert - je nach Datenbank-Anbieter - nicht nur einfache Werte wie Zahlen oder Zeichenketten sein können, sondern teilweise auch komplexe Inhalte wie binäre Dateien (Video, Bilder), öffnet sich der Anwendungsbereich. 

![KeyValue](./_res/KeyValue.png)

Im Beispiel werden verschiedene Datentypen gespeichert, wobei ein Wert **nur** über den Schlüssel (Key) abgerufen werden kann. Mit dem Schlüssel *Bild1* kriegen Sie also eine binäre Datei zurück, die wahrscheinlich ein Bild enthält. *addresse_1* würde also entsprechend ein JSON-Dokument enthalten.



## Wie werden die Daten gespeichert

Die Key-Value-Datenbanken sind ausserordentlich schnell - speziell da nur der Key für die Abfrage relevant ist. Die Datenstruktur, die im Hintergrund verwendet wird, ist nicht neu und fand bereits in relationalen Datenbank Einsatz. 

Grundsätzlich werden alle Key-Value-Datenbanken mit B-Trees oder B+-Trees gespeichert. Bei den Quellen finden Sie ein gute Beispiele wie beide der Bäume aufgebaut werden. 

[Im speziellen Fall von der ***etcd*** Datenbank - die wir hier im Unterricht verwenden - werden die Daten mit einem B+-Tree auf der Festplatte gespeichert und in einem B-Tree im Speicher/RAM zwischengespeichert (Cache)](https://etcd.io/docs/v3.5/learning/data_model/). 

Eine relationale Datenbank verwendet die B-Trees zur Indexierung und kann so ebenfalls schnell an die entsprechenden Daten kommen. Da die relationale Datenbank aber auch für andere Fälle ausgelegt wird, kann man nicht gleich gut optimieren und ist darum nicht gleich schnell wie eine relationale Datenbank im Lesen der Daten.

![btrees](./_res/btrees.png)



## Use Cases

Key-Value Datenbanken können dann eingesetzt werden, wenn der Zugriff auf Inhalte **ausschliesslich** über den Schlüssel geschehen (lesen und schreiben). 

**Applikations-Konfiguration**

Wenn Sie Ihre Applikationssettings in einer Datenbank führen möchten, weil z.B. Ihre Applikation verteilt ist, wäre dies eine gute Möglichkeit. Die Settings werden jeweils nur über den Schlüssel gelesen und in die Applikation eingebunden.

**Zustand und Informationen**

Sie können auch Status-Informationen in einer Key-Value-Datenbank speichern. Dies ermöglicht einer verteilten Applikation die Information zu lesen. Kubernetes verwendet **etcd** auf diese Art und speichert Informationen wie aktueller Zustand, gewünschter Zustand, Konfigurationen und Laufzeitinformationen.

**Applikationslogik**

Es ist durchaus auch möglich gewisse Teile einer Applikationslogik mit einer Key-Value-Datenbank zu verwalten, z.B. einen Shopping-Cart. Einen Shopping-Cart würden Sie tendenziell immer als Gesamtes auslesen und aktualisieren. Ein Problem würde es dann darstellen, wenn Sie zu einem gewissen Zeitpunkt wissen möchten, ob ein spezifisches Produkt gerade gekauft wird. Dann müssten Sie im Cart nach diesem Produkt suchen. Eine Key-Value-Datenbank ist nicht dafür ausgelegt, aber möglich ist es trotzdem.

![cart](./_res/cart.png)

Wenn Sie nun das Beispiel sehen und merken, dass die Daten wahrscheinlich redundant sind, weil der Name und die URL des Bildes ja sicherlich bereits in der Produkt-Tabelle der Produkt-Datenbank gespeichert sind, haben Sie recht. In den NoSql-Datenbanken werden oft Redundanzen eingeführt zugunsten der Performance. Es macht weniger Sinn, wenn Sie eine sehr schnelle Datenstruktur wählen und dann aber mit einer langsameren Struktur abgleichen müssen. 



## Populäre Key-Value-Datenbanken

- [etcd](https://etcd.io/)
- [Amazon DynamoDB](https://aws.amazon.com/pm/dynamodb/)
- [Azure Cosmos DB](https://azure.microsoft.com/en-us/products/cosmos-db/)
- [Redis](https://redis.com/)
- [Viele andere...](https://db-engines.com/en/ranking/key-value+store)



## Quellen

- 1 [Amazon: What Is a Key-Value Database?](https://aws.amazon.com/nosql/key-value/) 
- 2 [TechTarget: NoSQL database types explained: Key-value store](https://www.techtarget.com/searchdatamanagement/tip/NoSQL-database-types-explained-Key-value-store) 
- 3 [Baeldung: The Difference Between B-trees and B+trees](https://www.baeldung.com/cs/b-trees-vs-btrees)



