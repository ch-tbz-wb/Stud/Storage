# MongoDB Installation

[TOC]

## Installation

Sie verwenden AWS Ubuntu-Instanzen. Lesen Sie die entsprechende [Anleitung von MongoDB](https://www.mongodb.com/docs/manual/tutorial/install-mongodb-on-ubuntu/), falls Sie die Installation selbstständig vornehmen möchten. 

Es ist bereits eine [Cloud-Init-Konfiguration](install.yaml) verfügbar, die Ihnen die Installation abnimmt. Stellen Sie sicher, dass Sie folgende Einstellungen in AWS haben:

- Ubuntu 22.04
- Erstellen Sie einen SSH-Key im GUI oder verwenden Sie einen bestehenden! Aber bitte behalten Sie den bestehenden Schlüssel in der Cloud-Init bei und fügen Sie Ihren noch hinzu.
- Kleine Instanz-Typ Grösse (Mikro reicht)
- Speichergrösse: 20GB
- Fügen Sie eine statische IP hinzu, damit Sie nicht jedes mal Ihre Verbindung anpassen müssen.
- Verwenden Sie das oben verlinkte Cloud-Init-Skript, **aber passen Sie das Passwort an**!
- Öffnen Sie die den Port 27017 in der Firewall/Sicherheitsgruppe in AWS. 



## Überprüfung der Installation

1. Warten Sie bis die Installation abgeschlossen ist. Überprüfen Sie die Cloud-Init Log-Datei: */var/log/cloud-init-output.log*
2. Testen Sie, ob Sie sich mit der MongoDB verbinden können mit Ihren Benutzerangaben: `sudo mongosh -u admin`



