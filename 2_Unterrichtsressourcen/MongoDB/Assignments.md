# MongoDB Aufträge

[TOC]

Im Folgenden werden Aufträge definiert, welche Ihnen helfen sollen, sich in das Thema zu vertiefen. Mit jedem Teil kann man sich mehr oder weniger stark beschäftigen und so entsprechendes Know-How aneignen. 

**Ziele:** 

- Sie können ein Datenmodell für Ihre Datenbank erstellen
- Sie kennen die verschiedenen Arten wie Sie die Collections modellieren können aufgrund eines konzeptionellen Modells
- Sie können *MongoDB* installieren.
- Sie können in MongoDB Benutzer und Rollen verwalten.
- Sie kennen die Skalierungsvarianten und Konzepte
- Sie kennen die ACID, CAP und BASE Theoreme und können Sie auch erklären.
- Sie können die MongoDB Query Language (MQL) anwenden. 
- Sie können MongoDB mit Python ansprechen und Daten auslesen und manipulieren.

**Erweiterte Ziele:**

- Sie können JSON-Schemas zur Validierung in MongoDB verwenden. 
- Sie können Backups anwenden.
- Sie können einen Cluster installieren.



## A) Cloud-Init Installation

Installieren Sie [*MongoDB* nach dieser Anleitung](Installation.md) unter Verwendung von Cloud-Init. 

Sie werden in den folgenden Teilen eine konkrete Datenbank aufbauen und diese jeweils als Testobjekt verwenden. Überlegen Sie sich ein Thema, für welches Sie eine Datenbank erstellen möchte - ohne noch konkrete Inhalte zu definieren.

- Verbinden Sie sich via Command-Line Tool auf dem Server
- Verbinden Sie sich via [MongoDB Compass](https://www.mongodb.com/products/tools/compass) (grafisches UI). 



## B) Sicherheit

Schauen Sie sich das Cloud-Init nochmals genauer an. Es wird ein Benutzer erstellt mit Admin-Rechten und die Sicherheit aktiviert. Dieser Benutzer wird in der Datenbank *admin* gespeichert. Tatsächlich können Sie Benutzer in beliebigen Datenbanken speichern. Testen Sie verschiedene Einstellungen und deren Verhalten, z.B.

- Erstellen Sie einen Benutzer in der admin-Datenbank mit Rechten auf eine spezifische andere Datenbank
- Erstellen Sie einen Benutzer in einer beliebigen Datenbank mit Admin-Rechten
- Erstellen Sie eigene Rollen
- etc

Wenn Sie sich wohl im Umgang mit den Benutzern fühlen, erstellen Sie die Datenbank für Ihr Thema. 



## C) Modell

**Konzeptionelles Modell**

Erstellen Sie zuerst ein **konzeptionelles Modell** Ihres Themas und lassen Sie es **von der Lehrperson validieren** bevor Sie weiterfahren. Das Modell sollte eine gewissen Grösse haben, so dass die verschiedenen Beziehungstypen platz finden.

**Logisches Modell**

Erstellen Sie nun das [logische Modell](Datenmodell.md) für Ihr Thema und machen Sie die gewählte Variante sichtbar (textuell oder graphisch). Sie können zuerst auch mit "Kästchen" arbeiten, wenn Sie möchten, sollen aber definitiv auch JSON-Schemas produzieren für jede Collection. 



## D) Daten befüllen

[Erstellen Sie ein Script, welches Ihre Datenbank befüllt](https://gitlab.com/ch-tbz-wb/Stud/NoSQL/-/blob/main/2_Unterrichtsressourcen/MongoDB/Abfragen.md). Sie sollen dabei explizit die Methoden *insertOne*, *insertMany*, *updateOne*, *updateMany*, *replaceOne* verwenden. Bei den Update-Methoden sind einschränkende Filter explizit auch gewünscht. Natürlich könnten Sie auch nur Insert-Methoden verwenden, aber Sie sollen ja auch den Umgang mit Update-Methoden üben. 

Stellen Sie sicher, dass Sie mit Variablen arbeiten und nicht die gleichen Werte mehrmals manuell einfügen müssen.

Ihre Datenbank muss also sicherlich ca 10 Einträge pro Collection besitzen, so dass wir eine solide Datenbasis besitzen. 

Erstellen Sie auch ein Script, welches Ihre Datenbank reinigt und alles löscht. Dies sollte ja kein Problem sein, da Sie ein Script haben, welches wieder Daten einfügt. Verwenden Sie beide Methoden *deleteOne* und *deleteMany*.

Testen Sie Spezialfälle und dokumentieren Sie was geschieht:

- Löschen (oder Einfügen, Aktualisieren) Sie Daten mit Hilfe der Methode *deleteOne*, aber geben Sie den Filter so an, dass **mehrere Dokumente** gefunden würde. 
- Löschen (oder Einfügen, Aktualisieren) Sie Daten mit einem Filter, welche **keine Dokumente** zurück liefert. 
- etc



## E) Abfragen

[Erstellen Sie ein Script, welches die nachfolgenden Abfragen enthält](https://gitlab.com/ch-tbz-wb/Stud/NoSQL/-/blob/main/2_Unterrichtsressourcen/MongoDB/Abfragen.md). Dokumentieren Sie die Abfragen, z.B. in der folgenden Form 

```javascript
// Suche nach dem Wert "aa" im Feld xx
db.find({xx: "aa"});
// andere Abfragen...
db.find({});

```

**Einfache Abfragen**

Erstellen Sie zuerst einfache Abfragen, die nur auf einzelne Collections zugreifen. Verwenden Sie dazu die Methode *find* . Testen Sie u.a. folgende Fälle:

- Verwenden Sie ODER- und UND-Verknüpfungen und kombinieren Sie diese auch
- Geben Sie nur einen Teil der Felder zurück. Wie verhält sich das Feld *_id* ?

**Achtung**: Inhalte von Unter-Dokumenten werden später abgefragt. Konzentrieren Sie sich hier auf die Basis.



**Aggregationen**

Wenden Sie Aggregationen an, z.B. Gruppierung und/oder Summierung von Daten. Sie können auch verschiedene Aggregationen in mehreren *Stages* anwenden. Verwenden Sie die Stages `$match, $group, $project, $sort, $limit, $sum, $lookup, $unwind` nach belieben - vor allem auch in Kombination.

Viele der Aggregationen sind vielfältig anwendbar. Spielen Sie mit den Einstellungen, z.B.:

- `$project` kann einfach Felder ein- ausblenden oder Sie können weitergehen und Felder umbenennen und Texte formattieren.
- `$sum` können Sie für ein Count oder ein Sum verwenden. Finden Sie heraus wie?
- mit `$lookup` und anschliessendem `$unwind` können Sie Ihre joins "flach" ausgeben - ohne Unterdokumente.
- Filterung und joins auf Unterdokumenten oder sogar Arrays ist anspruchsvoll. Überlegen Sie sich zu welchem Zeitpunkt Sie besser flache oder verschachtelte Strukturen benötigen.



## F) Python Code  

Überführen Sie das gelernte nun in eine kleine [Applikation in Python](https://www.mongodb.com/languages/python/pymongo-tutorial). Das verlinkte Python-Modul sollte Ihnen bei den Abfragen helfen. Überlegen Sie sich zuerst wie ein entsprechender Code (oder Menu-Führung) aussehen könnten. Halten Sie Rücksprache mit der Lehrperson. 

Verzichten Sie auf komplizierte User Interfaces, der Fokus liegt auf dem Austausch mit MongoDB. Sie könnten z.B. ein einfaches Text-UI erstellen um Einträge anzuzeigen, zu erstellen und um zu aktualisieren.



## G) Erweiterung: Backup und Restore

Verwenden Sie *mongodump* und *mongorestore* um Backups zu erstellen und wiederherzustellen. Sie können diesen Task beliebig erweitern, z.B.

- Erstellen Sie einen Cron-Job, der das Backup automatisch ausführt
- Überlegen Sie sich wohin das Backup gespeichert werden sollte und führen Sie es dorthin im Script
- Steigen Sie um auf Snapshots der virtuellen Instanzen. Auch hier können Sie Cron-Jobs einrichten, die Ihre Backups erstellt und auch nach einer gewissen Zeit wieder löscht (z.B. mit Hilfe von AWS Lambda und CloudWatch). 



## H) Erweiterung: MongoDB-Validierung

Sie haben bereits JSON-Schemas erstellt. Verwenden Sie diese nun, um auf der Datenbank Ihre [Daten zu validieren](https://www.mongodb.com/docs/manual/core/schema-validation/specify-json-schema/). Sie können das Schema auch nachträglich einfügen, z.B. mit Hilfe von MongoDB Compass.

Testen Sie nun was passiert, wenn Ihr einzufügendes Dokument nicht korrekt ist. 



## I) Erweiterung: Replication & Cluster

Suchen Sie nach entsprechenden Informationen. Sie können das [MondoDB-Manual](https://www.mongodb.com/docs/manual/administration/replica-set-deployment/) als Startpunkt verwenden. 

Sie können gerne noch weiter gehen und auch einen [Shared Cluster installieren](https://www.mongodb.com/docs/manual/sharding/).



