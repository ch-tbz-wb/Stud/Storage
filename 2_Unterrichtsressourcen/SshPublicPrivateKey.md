# SSH mit Public/Private Key

[TOC]

Dieses Dokument erklärt wie Sie mit Public und Private Key im Zusammenhang mit SSH verwenden, aber nicht wie Public und Private Keys funktionieren. 

## SSH

SSH (Secure Shell oder Secure Socket Shell), ist ein Protokoll, um eine sichere Verbindung zu einem andere Gerät aufzubauen. Die Verbindung können Sie sowohl mit Passwort als auch mit Public/Private Key Verfahren aufbauen. 

In diesem Modul **verzichten** wir auf Passworte und arbeiten **ausschliesslich** mit Public/Private Keys. Dies aus Sicherheitsgründen. Wie Sie Public/Private Key-Paare erstellen, können Sie in dem nachfolgenden Kapitel nachlesen.

Der Befehl in **CMD/Powershell von Windows oder Bash von Linux/Mac** für den Zugriff sieht nun folgendermassen aus. Der Parameter *-o ServerAliveInterval=30* verhindert, dass Sie ein Timeout kriegen, ist aber für den Verbindungsaufbau nicht notwendig.

```bash
ssh <user>@<server> -i <path-to-privatekey>\<private-key-file>.pem -o ServerAliveInterval=30
# Standard-Benutzername für ubuntu-Systeme ist ubuntu!
# Standard-Benutzername für amazon-Instanzen ist ec2-user!
# Beispiel: ssh ubuntu@121.12.3.1 -i c:\Users\maxmuster\.ssh\maxmuster.pem -o ServerAliveInterval=30
```

**Quellen**:

Techtarget: [Secure Shell (SSH)](https://www.techtarget.com/searchsecurity/definition/Secure-Shell)

Thorntech: [Passwords vs. SSH keys - what's better for authentication?](https://thorntech.com/passwords-vs-ssh)



## Public / Private Key Paare

Public und Private Key-Paare gehören immer zusammen und gehören in den Bereich der Kryptographie. Ein Stück Code, eine Datei oder Verbindung kann mit dem öffentlichen Schlüssel verschlüsselt werden, aber nur mit dem privaten Schlüssel entschlüsselt. Sie können den öffentlichen Schlüssel daher jederzeit beliebigen Personen geben, ohne dass Sie in die Gefahr laufen, dass diese Personen Ihre Daten abrufen kann, die mit eben diesem öffentlichen Schlüssel verschlüsselt wurden.

Der private Schlüssel hingegen muss **gut geschützt werden** und sollte **jederzeit** nur auf Ihrem eigenen Rechner liegen! 

Im Umgang mit Cloud-Servern gilt folgender Grundsatz:

- **Auf dem Server** liegt der **öffentliche** Schlüssel (public key). Es spielt keine Rollen wer diesen Schlüssel sonst noch sieht.
- **Auf Ihrem Rechner** liegt der **private** Schlüssel (private key)

### Schlüsselpaare erstellen

Der private Schlüssel liegt grundsätzlich im Verzeichnis mit dem Namen *.ssh* in Ihrem Home-Verzeichnis. Dies sowohl unter Windows als auch MacOS.

```bash
# Schlüsselpaar erstellen
ssh-keygen -f C:\Users\<IhrBenutzer>\.ssh\<Keyname>.pem -t rsa -b 2048 -C max
# z.B.
# ssh-keygen -f C:\Users\maxmuster\.ssh\max.pem -t rsa -b 2048 -C max
```

Dieser Befehl erstellt sowohl den privaten als auch den öffentlichen Schlüssel. Parameter:

- -t: Verwendeter Algorithmus
- -b: Schlüssellänge. sollte mindestens 2048 sein.
- -C: Name des Schlüssels. Wird beim öffentlichen Schlüssel angehängt.

### Öffentlichen Schlüssel aus dem privaten Schlüssel extrahieren

Falls Sie bereits einen privaten Schlüssel haben, können Sie davon auch den öffentlichen Schlüssel exportieren.

```bash
ssh-keygen -y -f C:\Users\<IhrBenutzer>\.ssh\<keyname>.pem > C:\Users\<IhrBenutzer>\.ssh\<keyname>.pub
# z.B. 
# ssh-keygen -y -f C:\Users\Marco\.ssh\marco1.pem > C:\Users\Marco\.ssh\marco1.pub
```
