# ACID, CAP und BASE

[TOC]

## ACID

Die Abkürzung ACID steht für die notwendigen Eigenschaften, um Transaktionen auf Datenbankmanagementsystemen (DBMS) einsetzen zu können. Es steht für Atomarität (atomicity), Konsistenz (consistency), Isoliertheit (isolation) und Dauerhaftigkeit (durability).

#### Atomicity (Atomarität)

Ein Block von SQL-Anweisungen (eine oder mehrere) wird entweder ganz oder gar nicht ausgeführt: Das Datenbanksystem behandelt diese Anweisungen wie eine einzelne, unteilbare (daher atomare) Anweisungen. Gibt es bei einem Statement technische Probleme oder tritt ein definierter Zustand ein, der einen Abbruch erfordert (z. B. Kontoüberziehung), werden auch die bereits durchgeführten Operationen nicht auf der Datenbank wirksam.

**Beispiel:** Ein Kunde bestellt via Webshop ein Produkt. Der Warenkorb darf nur geleert werden, wenn gleichzeitig das Produkt bestellt werden kann.  

#### Consistency (Konsistenz)

Nach Abschluss der Transaktion befindet sich die Datenbank in einem konsistenten Zustand. Das bedeutet, dass Widerspruchsfreiheit der Daten gewährleistet sein muss. Das gilt für alle definierten Integritätsbedingungen genauso, wie für die Schlüssel- und Fremdschlüsselverknüpfungen.
Datenbanken erstellen und Daten einfügen Tabellentypen und Transaktionen

**Beispiel:** Ein Kunde verwendet ausschliesslich einen Gutschein als Zahlungsmittel. Sein Warenkorb-Wert darf beim Abschluss der Bestellung den Betrag auf dem Gutschein nicht überschreiten. Dies wäre ein Inkonsistenter Zustand

#### (Isolation) Isoliertheit

Die Ausführungen verschiedener Datenbankmanipulationen dürfen sich nicht gegenseitig beeinflussen. Mittels Sperrkonzepten oder Timestamps wird sichergestellt, dass durch eine Transaktion verwendete Daten nicht vor Beendigung dieser Transaktion verändert werden. Die Sperrungen müssen so kurz und begrenzt wie möglich gehalten werden, da sie die Performance der anderen Operationen beeinflusst.

**Beispiel:** 10 Kunden bestellen gleichzeitig ein Produkt, welches aber nur 9 mal im Lager ist. Es muss sichergestellt sein, dass auch nur 9 Kunden das Produkt bestellen können. 

#### Durability (Dauerhaftigkeit)

Nach Abschluss einer Transaktion müssen die Manipulationen dauerhaft in der Datenbank gespeichert sein. Der Pufferpool speichert häufig verwendete Teile einer Datenbank im Arbeitsspeicher. Diese müssen aber nach Abschluss auf der Festplatte gespeichert werden.

**Beispiel:** Ein Kunde bestellt via Webshop ein Produkt. Während des Bestellprozesses stürzen die Server ab. Der Zustand der Datenbank (und damit auch der der Zustand der Applikation) muss Konsistent bleiben. Es dürfen also keine Informationen verloren gehen.

## CAP-Theorem

Das CAP-Theorem behandelt verteilte System und die drei Eigenschaften Consistency, Availability und Partition. Das Theorem besagt, dass nicht alle drei Eigenschaften gleichzeitig erfüllt werden können.

#### Consistency (Konsistenz)

"Die Konsistenz der gespeicherten Daten. In verteilten Systemen mit replizierten Daten muss sichergestellt sein, dass nach Abschluss einer Transaktion auch alle Replikate des manipulierten Datensatzes aktualisiert werden. Diese Konsistenz sollte nicht verwechselt werden mit der Konsistenz der [ACID](https://de.wikipedia.org/wiki/ACID)-Transaktionen, die nur die innere Konsistenz eines Datenbestandes betrifft." (de.wikipedia.org)

#### Availability (Verfügbarkeit)

"Die Verfügbarkeit im Sinne akzeptabler Antwortzeiten. Alle Anfragen an das System werden stets beantwortet."  (de.wikipedia.org)

#### Partition (Partitionstoleranz/Ausfalltoleranz)

Das System arbeitet auch, wenn Teile des verteilten Systems ausfällt, z.B. durch Verlust des Netzwerkes zwischen Knoten des verteilten Systems.

#### Beweisführung

Der Beweis, dass nicht alle Eigenschaften gleichzeitig erfüllt werden können [wird hier sehr gut illustriert](https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/). 

## BASE

BASE steht - wie ACID -  für Konsistenzeigenschaften, welche von NoSQL-Datenbank-Typen verfolgt werden. Im Unterschied zu ACID, sind die Eigenschaften gelockert. 

Der Grund für die Lockerungen ist das **CAP-Theorem** welches **verteilte Systeme** behandelt.

#### Basically Available

"Systeme, die auf das BASE-Modell setzen, versprechen, hochverfügbar zu sein und zu jeder Zeit auf Anfragen reagieren zu können. Dafür machen sie Abstriche bei der Konsistenz (siehe dazu auch CAP-Theorem). Das bedeutet, dass ein System immer auf eine Anfrage reagiert. Die Antwort kann aber auch ein Fehler sein oder einen inkonsistenten Datenstand liefern" (wi-wiki.de)

#### Soft State

"Der Zustand, in dem noch nicht alle Änderungen an das gesamte System propagiert wurden, aber trotzdem auf Anfragen reagieren kann, nennt man auch *Soft State*" (wi-wiki.de)

#### Eventually Consistency

"Im Gegensatz zum strengen ACID-Modell wird in BASE die Konsistenz als eine Art „Übergang“ betrachtet, sodass nach einem Update nicht sofort alle Partitionen aktualisiert werden, sondern erst nach einer gewissen Zeit. Das System wird also „**letztendlich konsistent**“ (*eventually consistent*), aber bis dahin in einem inkonsistenten Übergangs-Zustand (*soft state*) sein." (wi-wiki.de)

## Weitere Überlegungen

ACID und BASE stehen nicht allen Bereichen per se im Gegensatz zueinander. Es können beide Modelle implementiert werden. Sie müssen aber verstehen wie ihr System im Hintergrund aufgebaut ist (Infrastruktur).

Wenn sie es nicht mit einem verteilten System zu tun haben, können sie auch mit NoSQL ACID einhalten. Tatsächlich [unterstützt MongoDb zum Beispiel Transaktionen](https://www.mongodb.com/basics/acid-transactions).  Sie können daher also jederzeit atomare Abfragen erstellen, die auch die anderen Eigenschaften von ACID erfüllen. 

Sobald ihr System aber auf ein verteiltes System erweitert wird, kann es sein, dass sie nicht die gleichen Daten lesen wie Sie sie eben geschrieben haben. Nun kommt das BASE-Modell zum Einsatz. 

Es geht im BASE-Modell also nicht darum, dass die Daten, die geschrieben werden per se nicht konsistent sind, sondern die Herausforderung steht im Zusammenhang mit einem verteilten System. Es kann sein, dass auf ein System geschrieben wird (konsistent), aber dann später von einem anderen System gelesen wird auf dem die Daten noch nicht aktualisiert wurden.

## Quellen und Weiterführende Literatur

- <https://www.mongodb.com/basics/acid-transactions>
- <http://wi-wiki.de/doku.php?id=bigdata:konsistenz>
- <https://de.wikipedia.org/wiki/CAP-Theorem>
- <https://mwhittaker.github.io/blog/an_illustrated_proof_of_the_cap_theorem/>
- 
