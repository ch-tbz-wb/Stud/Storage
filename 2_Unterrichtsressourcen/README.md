# NoSql Datenbanken

[TOC]

## Lektionenübersicht

Die Themen werden zu grossen Teilen selbstständig erarbeiten auf Grund von Aufträgen, die aber auch selbstständig erweitert werden können. Zusätzlich führen wir einzelne Konzepte im Plenumsunterricht ein. Die geplanten Plenumsinputs sind wie folgt. Änderungen werden von der Lehrperson frühzeitig kommuniziert. 

**Tag 01**:

- Einführung NoSql Datenbanken
- Arbeitsumgebung kennenlernen
- Einführung Key-Value Datenbanken (etcd)

**Tag 03**:

- Einführung Zeitreihen Datenbanken (Prometheus)

**Tag 05**:

- ACID, CAP und BASE
- Einführung Document Store (MongoDb)

**Tag 10**:

- Abschluss

## Themen

### Allgemein

[SSH mit Public/Private Key](SshPublicPrivateKey.md)

[ACID / CAP / BASE](./ACID_CAP_BASE.md)

### etcd: Key-Value Datenbank

[Konzepte](./etcd/Konzepte.md)

[Installation](./etcd/Installation.md)

[Aufträge](./etcd/Assignments.md)

### Prometheus: Zeitreihen Datenbank

[Konzepte](./Prometheus/Konzepte.md)

[Installation](./Prometheus/Installation.md)

[Aufträge](./Prometheus/Assignments.md)

### MongoDB: Document-Store-Datenbank 

[Installation](./MongoDB/Installation.md)

[Administration](./MongoDB/Administration.md)

[Datenmodellierung](./MongoDB/Datenmodell.md)

[Datenmanipulation und Datenabfragen](./MongoDB/Abfragen.md)

[Aufträge](./MongoDB/Assignments.md)

