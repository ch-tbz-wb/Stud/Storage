# Handlungskompetenzen

## Kompetenzmatrix

| Kompetenzband                                                | RP           | HZ   | Novizenkompetenz                                             | Fortgeschrittene Kompetenz                                   | Kompetenz professionellen Handelns                           | Kompetenzexpertise                                           |
| ------------------------------------------------------------ | ------------ | ---- | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ | ------------------------------------------------------------ |
| a) NoSql Datenbank Familien kennenlernen und Anwendungszwecke verstehen | B11.7        | 1    | Kennt grundlegende NoSQL-Datenbanktypen und kann diese unterscheiden. | Kann die Merkmale verschiedener NoSQL-Datenbanktypen erläutern und deren Anwendungszwecke benennen. | Ist in der Lage, eine fundierte Empfehlung zur Auswahl einer geeigneten NoSQL-Datenbank für spezifische Anwendungsfälle zu geben. | Entwickelt optimierte Strategien für die Nutzung und Skalierung von NoSQL-Datenbanken in komplexen Systemen und groß angelegten Projekten. |
| b) Key-Value Datenbanken verstehen und anwenden              | B11.7, B13.1 | 2    | Kennt das grundlegende Konzept dieses Datenbank-Typs         | Kann die Funktionsweise und typische Anwendungen dieses Datenbank-Typs erklären. | Kann diesen Datenbanktyp installieren, verwalten und mit einer Programmiersprache darauf zugreifen | Entwickelt maßgeschneiderte Lösungen für hochkomplexe Anwendungen für diesen Datenbank-Typ und optimiert deren Performance und Skalierbarkeit. |
| c) Timeseries Datenbanken verstehen und anwenden             | B11.7, B13.1 | 3    | Kennt das grundlegende Konzept dieses Datenbank-Typs         | Kann die Funktionsweise und typische Anwendungen dieses Datenbank-Typs erklären. | Kann diesen Datenbanktyp installieren, verwalten und mit einer Programmiersprache darauf zugreifen | Entwickelt maßgeschneiderte Lösungen für hochkomplexe Anwendungen für diesen Datenbank-Typ und optimiert deren Performance und Skalierbarkeit. |
| d) Dokument-basierte Datenbanken verstehen und anwenden      | B11.7, B13.1 | 4    | Kennt das grundlegende Konzept dieses Datenbank-Typs         | Kann die Funktionsweise und typische Anwendungen dieses Datenbank-Typs erklären. | Kann diesen Datenbanktyp installieren, verwalten und mit einer Programmiersprache darauf zugreifen | Entwickelt maßgeschneiderte Lösungen für hochkomplexe Anwendungen für diesen Datenbank-Typ und optimiert deren Performance und Skalierbarkeit. |
| e) Graph-Datenbanken verstehen und anwenden                  | B11.7, B13.1 | 5    | Kennt das grundlegende Konzept dieses Datenbank-Typs         | Kann die Funktionsweise und typische Anwendungen dieses Datenbank-Typs erklären. | Kann diesen Datenbanktyp installieren, verwalten und mit einer Programmiersprache darauf zugreifen | Entwickelt maßgeschneiderte Lösungen für hochkomplexe Anwendungen für diesen Datenbank-Typ und optimiert deren Performance und Skalierbarkeit. |

RP = Rahmenlehrplan

HZ = Handlungsziele

## Modulspezifische Handlungskompetenzen

* B11: Applikationen entwickeln, Programme erstellen und testen
  * B11.7  Datenbanken aufgrund konzeptioneller Datenmodelle logisch abbilden und in Applikationen integrieren  (KN02)

* B13: Konzepte und Services entwickeln
  * B13.1  Archiv-, Backup-, Restore- und Repair-Konzepte für die Software, Datenbestände und Datenbanken erarbeiten.  (KN02)


## Anforderungsniveau

Das Anforderungsniveau einer Kompetenz ist durch die Komplexität der zu lösenden Problemstellung, die Veränderlichkeit und Unvorhersehbarkeit des Arbeitskontextes und die Verantwortlichkeit im Bereich der Zusammenarbeit und Führung definiert. HF Absolvierende sind generell in der Lage Problemstellungen und Herausforderungen zu analysieren, diese adäquat zu bewerten und mit innovativen Problemlösestrategien zu lösen

### Kompetenzniveau 1: Novizenkompetenz

Erfüllen selbständig fachliche Anforderungen; mehrheitlich wiederkehrende Aufgaben in einem überschaubaren und stabil strukturierten Tätigkeitsgebiet; Arbeit im Team und unter Anleitung.

### Kompetenzniveau 2: fortgeschrittene Kompetenz

Erkennen und analysieren umfassende fachliche Aufgabenstellungen in einem komplexen Arbeitskontext und sich veränderndem Arbeitsbereich; führen teils kleinere Teams; erledigen die Arbeiten selbständig unter Verantwortung einer Drittperson.

### Kompetenzniveau 3: Kompetenz professionellen Handelns

Bearbeiten neue komplexe Aufgaben und Problemstellungen in einem nicht vorhersehbaren Arbeitskontext; übernehmen die operative Verantwortung und planen, handeln und evaluieren autonom.

### Kompetenzniveau 4: Kompetenzexpertise

Entwickeln innovative Lösungen in einem komplexen Tätigkeitsfeld; antizipieren Veränderungen in der Zukunft und handeln proaktiv; übernehmen strategische Verantwortung und treiben Veränderungen und Entwicklungen voran.

