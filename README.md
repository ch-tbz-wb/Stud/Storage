![TBZ Logo](./x_gitressourcen/tbz_logo.png) 

# NOSQL - NoSQL, Key/Value, Time Series

## Kurzbeschreibung des Moduls 

Datenbanken sind weit verbreitet. Über lange Zeit waren relationale Datenbanken der berechtigterweise der Standard. Mit der Zunahme von Cloudtechnologien, verteilten Systemen und grossen Lasten wurden aber weitere Datenbanktypen entwickelt, die den neuen Anforderungen standhalten. In diesem Modul werden Sie mehr über die NoSql-Datenbanken und die verschiedenen Familien erfahren. Speziell zuwenden werden wir uns dem Platzhirsch *MongoDb* und dann auch Key-Value Speichertypen, weil die speziell in Cloud-Technologien Verwendung finden. 

## Angaben zum Transfer der erworbenen Kompetenzen 

Sie werden weitgehenden selbstständig und praktisch arbeiten. Vordefinierte Aufgaben werden durchgeführt und durch die Lehrperson unterstützt. 

## Abhängigkeiten und Abgrenzungen

### Parallele Module

Parallel zu diesem Modul besuchen Sie das SQL-Modul - also relationale Datenbanken und werden so die (teilweise grossen) Unterschiede direkt erleben.

## Unterlagen zum Modul

### Organisatorisches

[Organisatorisches ](0_Organisatorisches)zur Autorenschaft dieser Dokumente zum Modul

## Handlungskompetenzen

[Handlungskompetenzen](1_Handlungskompetenzen) zu den Handlungszielen 

### Unterrichtsressourcen

[Unterrichtsressourcen](2_Unterrichtsressourcen) von Aufträgen und Inhalten zu den einzelnen Kompetenzen

### Umsetzung

[Umsetzung](3_Umsetzung) 

### Fragekatalog

[Fragekatalog ](4_Fragekatalog) Allgemein

### Handlungssituationen

[Handlungssituationen ](5_Handlungssituationen)mit möglichen Praxissituationen zu den einzelnen Handlungszielen

### Zertifizierungen 

Mögliche [Zertifizierungen](8_Zertifizierungen) für dieses Modul 

- - -

<a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/"><img alt="Creative Commons Lizenzvertrag" style="border-width:0" src="https://i.creativecommons.org/l/by-nc-sa/3.0/ch/88x31.png" /></a><br />Dieses Werk ist lizenziert unter einer <a rel="license" href="http://creativecommons.org/licenses/by-nc-sa/3.0/ch/">Creative Commons Namensnennung - Nicht-kommerziell - Weitergabe unter gleichen Bedingungen 3.0 Schweiz Lizenz</a>.

- - -
