# Umsetzung

- Bereich: Datenbanken
- Semester: 3

## Lektionen: 

* Präsenzunterricht: 20
* Fernunterricht: 20
* Selbststudium: 20

## Fahrplan

| **Lektion** | **Selbststudium** | **Kompetenzbereich**                                         | Kompetenzband | **Tools**                                |
| ----------- | ----------------- | ------------------------------------------------------------ | ------------- | ---------------------------------------- |
| 4           | 0                 | NoSql Datenbank Familien kennenlernen und Anwendungszwecke verstehen | a)            |                                          |
| 10          | 6                 | Key-Value Datenbanken verstehen und anwenden                 | b)            | etcd, VS Code, AWS, Cloud-Init           |
| 10          | 6                 | Timeseries Datenbanken verstehen und anwenden                | c)            | Prometheus, VS Code, AWS, Cloud-Init     |
| 8           | 4                 | Dokument-basierte Datenbanken verstehen und anwenden         | d)            | MongoDB, VS Code, AWS, Cloud-Init, Atlas |
| 8           | 4                 | Graph-Datenbanken konzeptionell verstehen und anwenden       | e)            | Neo4J, VS Code, AWS, Cloud-Init          |

| **Total: 40** | **Total: 20** | **-** | **-** |

## Lernziele: 

- NoSql Datenbank Familien kennenlernen und Anwendungszwecke verstehen
- Key-Value Datenbanken konzeptionell verstehen, erklären und anwenden
- Timeseries Datenbanken konzeptionell verstehen, erklären und anwenden
- Dokument-basierte Datenbanken konzeptionell verstehen, erklären und anwenden
- Graph-Datenbanken konzeptionell verstehen, erklären und anwenden

## Voraussetzungen:

SQL Modul bereits besucht.

## Dispensation

Gemäss Reglement HF Lehrgang

## Technologien

etcd, Prometheus, MongoDB, AWS, MongoDB Compass, Neo4j, Visual Studio Code, JSON

## Methoden

Self-Learning Plattformen und praktische Übungen, Coaching durch Lehrperson

## Schlüsselbegriffe

NoSql, Datenbank, Key-Value, Zeitserien, Dokument-Datenbank, JSON, Graph Datenbanken

## Lehr- und Lernformen

Lehrervorträge, Lehrgespräche, Lernfragen,  Einzel- und Gruppenarbeiten, Präsentationen

## Lehrmittel:

Alle Lehrmittel sind in diesem Repository verlinkt.

## Hilfsmittel:





