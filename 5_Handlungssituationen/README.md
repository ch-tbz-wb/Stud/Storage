# Handlungsziele und Handlungssituationen 

### 1. NoSql Datenbank Familien kennenlernen und Anwendungszwecke verstehen

Die Lernenden sollen die verschiedenen NoSQL-Datenbankfamilien kennenlernen und deren typische Anwendungszwecke verstehen.

**Handlungssituation:** Ein Unternehmen plant die Einführung einer neuen Datenbank für seine wachsenden Datenmengen und bittet dich, verschiedene NoSQL-Datenbankfamilien vorzustellen. Ziel ist es, die passende Datenbanklösung für Anwendungsbereiche wie Produktkataloge, Kundeninformationen und Log-Daten zu finden. Deine Aufgabe besteht darin, die relevanten NoSQL-Typen zu präsentieren, Unterschiede zu relationalen Datenbanken zu erläutern und eine Empfehlung basierend auf den spezifischen Anforderungen zu formulieren.

### 2. Key-Value Datenbanken verstehen und anwenden

Die Lernenden sollen die Struktur und Funktionsweise von Key-Value-Datenbanken konzeptionell verstehen, erklären und auf praktische Anwendungsbeispiele anwenden können.

**Handlungssituation:** Du arbeitest in einem Team, das eine E-Commerce-Anwendung entwickelt, bei der Benutzer-Session-Daten effizient gespeichert werden sollen, um schnelle Zugriffe und eine hohe Skalierbarkeit zu gewährleisten. Deine Aufgabe ist es, die Verwendung einer Key-Value-Datenbank für die Speicherung dieser Sitzungsdaten zu konzipieren und dem Team die Funktionsweise und Vorteile dieser Datenbankstruktur zu erklären, um die Anwendungsperformance zu optimieren.

### 3. Timeseries Datenbanken verstehen und anwenden

Die Lernenden sollen die Besonderheiten und Einsatzgebiete von Timeseries-Datenbanken konzeptionell verstehen, deren Nutzen für die Verarbeitung von Zeitreihendaten erklären und in Beispielen anwenden können.

**Handlungssituation:** Ein Unternehmen betreibt mehrere Server und Anwendungen, die kontinuierlich überwacht werden sollen, um Ausfälle frühzeitig zu erkennen und die Systemperformance zu optimieren. Du wirst beauftragt, Prometheus als Timeseries-Datenbank einzuführen, um Metriken wie CPU-Auslastung, Speicherverbrauch und Netzwerkaktivitäten über Zeit zu erfassen und zu analysieren. Deine Aufgabe ist es, das Monitoring-Konzept mit Prometheus zu entwerfen, dem Team die Funktionsweise der Timeseries-Datenbank zu erklären und die Vorteile für das Echtzeit-Monitoring sowie das Erkennen von Anomalien im System zu verdeutlichen.

### 4. Dokument-basierte Datenbanken verstehen und anwenden

Die Lernenden sollen dokument-basierte Datenbanken konzeptionell verstehen, deren Aufbau und Nutzen erläutern sowie Einsatzmöglichkeiten in der Praxis beschreiben können.

**Handlungssituation:** Ein Team aus der Softwareentwicklung arbeitet an einer Content-Management-Anwendung, bei der die Inhalte für verschiedene Webanwendungen flexibel und strukturiert gespeichert werden sollen. Du bist dafür verantwortlich, die Vorteile einer dokument-basierten Datenbank zu erläutern und das Team bei der Implementierung zu unterstützen, sodass unstrukturierte und strukturierte Daten effizient gespeichert und abgerufen werden können.

### 5. Graph-Datenbanken konzeptionell verstehen und anwenden

Die Lernenden sollen die Eigenschaften von Graph-Datenbanken konzeptionell verstehen, Anwendungsfälle identifizieren und die Vorteile für die Analyse von Netzwerken und Beziehungen erklären können.

**Handlungssituation:** Ein soziales Netzwerk möchte die Beziehungen zwischen den Nutzern analysieren, um Empfehlungen für neue Verbindungen zu generieren. Du erhältst die Aufgabe, eine Graph-Datenbank zu entwickeln, die diese Beziehungsmuster abbildet und das System so zu gestalten, dass Netzwerkverbindungen schnell und präzise analysiert werden können. Dabei sollst du dem Team die Besonderheiten und den Nutzen von Graph-Datenbanken für die Netzwerk- und Beziehungsanalyse erklären und bei der Modellierung des Datenschemas unterstützen.
